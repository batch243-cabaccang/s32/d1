const server = require("http");
const port = 8000;

const DUMMY_DIRECTORY = [
  { name: "Horhe", age: 19 },
  {
    name: "Nikki",
    age: 19,
  },
];

server
  .createServer((req, res) => {
    if (req.url == "/users" && req.method == "GET") {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.write(JSON.stringify(DUMMY_DIRECTORY));
      res.end();
      return;
    }
    if (req.url == "/users" && req.method == "POST") {
      let requestBody = "";
      req.on("data", (data) => {
        requestBody += data;
      });
      req.on("end", () => {
        requestBody = JSON.parse(requestBody);
        const newUser = {
          name: requestBody.name,
          age: requestBody.age,
        };

        DUMMY_DIRECTORY.push(newUser);
        console.log(DUMMY_DIRECTORY);

        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify(DUMMY_DIRECTORY));
        res.end();
      });
      return;
    }
  })
  .listen(port);
console.log(`Server Running on port ${port}`);
