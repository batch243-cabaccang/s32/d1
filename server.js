const server = require("http");
const port = 4000;

server
  .createServer((req, res) => {
    if (req.url == "/" && req.method == "GET") {
      res.end("get");
      return;
    }
    if (req.url == "/" && req.method == "POST") {
      res.end("post");
      return;
    }
  })
  .listen(port);
console.log(`Server Running on ${port}`);
